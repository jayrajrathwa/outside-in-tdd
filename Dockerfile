
FROM adoptopenjdk/openjdk11-openj9:jdk-11.0.1.13-alpine-slim
WORKDIR /app
COPY ./build/libs/outside-in-kata-0.1-all.jar ./app.jar
EXPOSE 8080
CMD ["java", "-jar", "app.jar"]