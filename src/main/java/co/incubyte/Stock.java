package co.incubyte;

public class Stock {
  private float price;
  private String name;
  private String ticker;

  public Stock() {}

  public Stock(String ticker, String name, float price) {
    this.ticker = ticker;
    this.name = name;
    this.price = price;
  }

  public boolean equals(Stock stock) {
    return stock.getTicker().equals(this.getTicker())
            && stock.getName().equals(this.getName())
            && stock.getPrice() == (this.getPrice());
  }

  public Stock(String ticker) {
    this.ticker = ticker;
  }

  public String getTicker() {
    return ticker;
  }

  public String getName() {
    return name;
  }

  public float getPrice() {
    return price;
  }

  public void setTicker(String ticker) {
    this.ticker = ticker;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setPrice(float price) {
    this.price = price;
  }
}
