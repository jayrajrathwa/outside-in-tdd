package co.incubyte.service;

import co.incubyte.Stock;
import co.incubyte.gateway.TickerGateway;
import jakarta.inject.Singleton;

import java.util.Locale;
import java.util.stream.Collectors;

@Singleton
public class TickerService {
  TickerGateway tickerGateway;

  public TickerService(TickerGateway tickerGateway) {
    this.tickerGateway = tickerGateway;
  }

  public Stock getByTicker(String ticker) {
    return tickerGateway.getTickers().stream()
        .filter(
            stock ->
                stock.getTicker().toLowerCase(Locale.ROOT).equals(ticker.toLowerCase(Locale.ROOT)))
        .collect(Collectors.toList())
        .get(0);
  }
}
