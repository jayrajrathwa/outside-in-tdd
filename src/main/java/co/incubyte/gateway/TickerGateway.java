package co.incubyte.gateway;

import co.incubyte.Stock;
import io.micronaut.core.type.Argument;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.annotation.Client;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;

import java.util.List;

@Singleton
public class TickerGateway {

  @Inject
  @Client("https://61bdb57a2a1dd4001708a0f3.mockapi.io/api/v1")
  HttpClient client;

  public List<Stock> getTickers() {
    return client.toBlocking().retrieve(HttpRequest.GET("/stocks"), Argument.listOf(Stock.class));
  }
}
