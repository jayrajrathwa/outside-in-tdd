package co.incubyte.controller;

import co.incubyte.Stock;
import co.incubyte.service.TickerService;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;

@Controller("/tickers")
public class TickerController {

  TickerService tickerService;

  public TickerController(TickerService tickerService) {
    this.tickerService = tickerService;
  }

  @Get("/{ticker}")
  public HttpResponse<Stock> getByTicker(String ticker) {

    try {
      return HttpResponse.ok(tickerService.getByTicker(ticker));
    } catch (Exception e) {
      return null;
    }
  }
}
