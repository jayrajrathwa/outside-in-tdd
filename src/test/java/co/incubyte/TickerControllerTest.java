package co.incubyte;

import io.micronaut.core.type.Argument;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static io.micronaut.http.HttpRequest.GET;

@MicronautTest
public class TickerControllerTest {
  @Inject
  @Client("/tickers")
  HttpClient client;

  @Test
  void should_return_not_found_for_invalid_ticker() {
    Assertions.assertThrows(HttpClientResponseException.class, () -> client.toBlocking().exchange(GET("/xyz"), Argument.of(Stock.class)));
  }
}
