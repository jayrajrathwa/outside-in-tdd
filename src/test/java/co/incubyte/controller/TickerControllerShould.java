package co.incubyte.controller;

import co.incubyte.service.TickerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class TickerControllerShould {

    TickerService tickerService;
    TickerController tickerController;

    @BeforeEach
    void init() {
        tickerService = mock(TickerService.class);
        tickerController = new TickerController(tickerService);
    }

    @Test
    void invoke_get_by_ticker_service() {
        tickerController.getByTicker("ZEEL");
        verify(tickerService).getByTicker("ZEEL");
    }
}
