package co.incubyte.service;

import co.incubyte.Stock;
import co.incubyte.gateway.TickerGateway;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class TickerServiceShould {
    TickerGateway tickerGateway;
    TickerService tickerService;

    @BeforeEach
    void init() {
        tickerGateway = mock(TickerGateway.class);
        Stock stock1 = new Stock("ZEEL", "Zee Entertainement Limited", 100);
        Stock stock2 = new Stock("paytm","One 97 Communications Limited",1500);
        List<Stock> stocks = new ArrayList<>();
        stocks.add(stock1);
        stocks.add(stock2);
        when(tickerGateway.getTickers()).thenReturn(stocks);
    }
    @Test
    public void invoke_get_tickers_and_return_given_ticker() {
        tickerService = new TickerService(tickerGateway);
        Stock stock = tickerService.getByTicker("ZEEL");
        verify(tickerGateway).getTickers();
        Stock expectedStock = new Stock("ZEEL", "Zee Entertainement Limited", 100);
        Assertions.assertTrue(expectedStock.equals(stock));
    }

    @Test
    public void throw_an_exception_if_not_found() {
        tickerService = new TickerService(tickerGateway);
        assertThrows(IndexOutOfBoundsException.class, () -> tickerService.getByTicker("xyz"));
    }
}
